/********************************************
 *  Oliver Ellis, Christian Marcel Michel   *
 *  Matrikelnummer: 491848, 492980          *
 *  Wissenschaftliches Rechnen mit C++      *
 *  Hausaufgabe 2: Farey-Folgen             *
 *  rational.cpp                            *
 * ******************************************/
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iostream>
#include "rational.h"

// Contructors
// Name: Rational
// Parameter 1: int numerator
// Parameter 2: int denominator
// Function: Constructor. Initialises a fraction with given numerator and denominator.
Rational::Rational(int numerator, int denominator)
{
    // Throw exception when denominator is zero
    if(denominator == 0) throw std::invalid_argument("The denominator must not be 0!");

    // Normalise the fraction and set values
    setRational(numerator, denominator);
}

// Name: Rational
// Parameter 1: int numerator
// Function: Constructor. Stores a whole number as a fraction.
Rational::Rational(int number) : Rational(number, 1)
{

}

// Name: Rational
// Function: Constructor. Initialises an object with a default value --> 1/1.
Rational::Rational() : Rational(1, 1)
{
    
}

// Operator overloading
// Name: operator==
// Parameter 1: Rational a
// Return: bool
// Function: Overloads the == operator to compare two Rational objects of equality.
bool Rational::operator==(Rational a)
{
    if(this->_numerator == a._numerator &&
       this->_denominator == a._denominator) return true;

    return false;
}

// Name: operator*=
// Parameter 1: Rational& a
// Return: Rational&
// Function: Overloads the *= operator to multiply two Rational objects.
Rational& Rational::operator*=(Rational& a)
{
    int newDenominator = this->_denominator * a._denominator;
    int newNumerator = this->_numerator * a._numerator;

    this->setRational(newNumerator, newDenominator);

    return *this;
}

// Name: operator*=
// Parameter 1: int& a
// Return: Rational&
// Function: Overloads the *= operator to multiply a rational object with a whole number.
Rational& Rational::operator*=(int& a)
{
    int newDenominator = this->_denominator;
    int newNumerator = this->_numerator * a;

    this->setRational(newNumerator, newDenominator);

    return *this;
}

// Name: operator/=
// Parameter 1: Rational& a
// Return: Rational&
// Function: Overloads the /= operator to divide two Rational objects.
Rational& Rational::operator/=(Rational& a)
{
    int newDenominator = this->_denominator * a._numerator;
    int newNumerator = this->_numerator * a._denominator;

    this->setRational(newNumerator, newDenominator);

    return *this;
}

// Name: operator+=
// Parameter 1: Rational& a
// Return: Rational&
// Function: Overloads the += operator to add two Rational objects.
Rational& Rational::operator+=(Rational& a)
{
    int newDenominator = this->_denominator * a._denominator;
    int newNumerator = (this->_numerator * a._denominator) + (a._numerator * this->_denominator);

    this->setRational(newNumerator, newDenominator);

    return *this;
}

// Name: operator+=
// Parameter 1: int& a
// Return: Rational&
// Function: Overloads the += operator to add a Rational object with a whole number.
Rational& Rational::operator+=(int& a)
{
    int newDenominator = this->_denominator;
    int newNumerator = (a * newDenominator) + this->_numerator;

    this->setRational(newNumerator, newDenominator);

    return *this;
}

// Name: operator-=
// Parameter 1: Rational& a
// Return: Rational&
// Function: Overloads the -= operator to add two Rational objects.
Rational& Rational::operator-=(Rational& a)
{
    int newDenominator = this->_denominator * a._denominator;
    int newNumerator = (this->_numerator * a._denominator) - (a._numerator * this->_denominator);

    this->setRational(newNumerator, newDenominator);

    return *this;
}

// Public methods
// Name: gcd --> Greatest COmmon Divisor
// Parameter 1: int a
// Parameter 2: int b
// Return: int
// Function: A static method which computes the greatest common divisor of two whole numbers.
int Rational::gcd(int a, int b)
{
    // 0. Termination of the recursion
    if(a == 0) return b;
    if(b == 0) return a;

    // 1. a is greater than b
    if(a > b) return gcd(a - b, b);

    // 2. b is greater or equal a
    else return gcd(a, b - a);
}

// Name: print
// Return: string
// Function: Returns a string showing a rational number as a fraction --> numerator/denominator.
std::string Rational::print()
{
    std::stringstream nomeratorStream;
    nomeratorStream << _numerator;
    
    std::stringstream denominatorStream;
    denominatorStream << _denominator;


    std::string numerator = nomeratorStream.str();
    std::string denominator = denominatorStream.str();
    std::string fraction = numerator + "/" + denominator;

    return fraction;
}

void Rational::Farey(int n)
{
    if(n > 0)
    {
        Farey_(n);
        std::cout << "n = " << n << ": " << printFarey() << std::endl;
        clearFarey();
    }
    else throw std::invalid_argument("Only positiv values can be used!");
}

// Name printFarey
// Return: string
// Function: Returns the computed Farey-Sequence as string if _fareySequence is not empty, else you get an empty string.
std::string Rational::printFarey()
{
    std::string sequence = "";
    
    if(_fareySquence.size() > 0)
    {
        for(int i = 0; i < _fareySquence.size(); i++)
        {
            sequence = sequence + _fareySquence[i].print() + ", ";
        }
    }

    return sequence;
}

// Name: clearFarey
// Function: Cleans _fareySequence.
void Rational::clearFarey()
{
    _fareySquence.clear();
}

// Private methods
// Name: setRational
// Parameter 1: int numerator
// Parameter 2: int denominator
// Function: Cancels down a fraction and sets the new value for the this-object.
void Rational::setRational(int numerator, int denominator)
{
    int newNumerator = 0;
    int newDenominator = 0;

    // Only absolute values can be computed in GCD
    if(numerator < 0) newNumerator = numerator * -1;
    else newNumerator = numerator;

     // Only absolute values can be computed in GCD
    if(denominator < 0) newDenominator = denominator * -1;
    else newDenominator = denominator;

    int gcd = Rational::gcd(newNumerator, newDenominator);

    // Cancel down the fraction
    numerator = numerator / gcd;
    denominator = denominator / gcd;

    this->_numerator = numerator;
    this->_denominator = denominator;
}

// Name Farey_
// Parameter 1: int n
// Function: Computes recursively a Farey-Sequence to the level of a given integer.
void Rational::Farey_(int n)
{
    // 0. n must not be negative
    if(n < 0) throw std::invalid_argument("Only positive values can work!");

    // 1. Termination after last step
    if(n == 0)
    {
        Rational f1(0, 1);
        Rational f2(1, 1);
        _fareySquence.push_back(f1);
        _fareySquence.push_back(f2);
    }
    else
    {
        // 2. Recursive step
        this->Farey_(n -1);
        int size = _fareySquence.size();

        for(int i = 0; i < size; i++)
        {
            int numerator = _fareySquence[i].numerator() + _fareySquence[i + 1].numerator();
            int denominator = _fareySquence[i].denominator() + _fareySquence[i + 1].denominator();

            if(denominator == n)
            {
                Rational f(numerator, denominator);
                _fareySquence.push_back(f);
            }
        }

        // Sort the list correctly
        std::sort(_fareySquence.begin(), _fareySquence.end());

        // Eliminate duplicates
        _fareySquence.erase(std::unique(_fareySquence.begin(), _fareySquence.end()), _fareySquence.end());
    }
}