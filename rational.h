/********************************************
 *  Oliver Ellis, Christian Marcel Michels  *
 *  Matrikelnummer: 491848, 492980          *
 *  Wissenschaftliches Rechnen mit C++      *
 *  Hausaufgabe 2: Farey-Folgen             *
 *  rational.h                              *
 * ******************************************/
#include <string>
#include <vector>

// Name Rational
// Function: Represents rational number by storing the numerator and the denominator.
class Rational
{
private: // Fields
    int _numerator;
    int _denominator;
    std::vector<Rational> _fareySquence;

public: // Constructors
    Rational(int numerator, int denominator);
    Rational(int number);
    Rational();

public: // Operator overloading
    bool operator==(Rational a);
    Rational& operator*=(Rational& a);
    friend inline Rational operator*(Rational a, Rational b) { return a *= b; }
    Rational& operator*=(int& a);
    friend inline Rational operator*(Rational a, int b) { return a *= b; }
    friend inline Rational operator*(int a, Rational b) { return b *= a; }
    Rational& operator/=(Rational& a);
    friend inline Rational operator/(Rational a, Rational b) { return a /= b; }
    Rational& operator+=(Rational& a);
    friend inline Rational operator+(Rational a, Rational b) { return a += b; }
    Rational& operator+=(int& a);
    friend inline Rational operator+(Rational a, int b) { return a += b; }
    friend inline Rational operator+(int a, Rational b) { return b += a; }
    Rational& operator-=(Rational& a);
    friend inline Rational operator-(Rational a, Rational b) { return a -= b; }
    operator double ()  const { return (double) _numerator / _denominator; };
    bool operator()(const Rational& n1,const Rational& n2) { return n1<n2; }

public: // Methods
    int numerator() { return _numerator; }
    int denominator() { return _denominator; }
    std::string print();
    static int gcd(int a, int b);
    void Farey(int n);
    std::string printFarey();
    void clearFarey();

private: // Methods
    void setRational(int numerator, int denominator);
    void Farey_(int n);
};