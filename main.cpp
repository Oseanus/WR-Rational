/********************************************
 *  Oliver Ellis,Christian Marcel Michels   *
 *  Matrikelnummer: 491848, 492980          *
 *  Wissenschaftliches Rechnen mit C++      *
 *  Hausaufgabe 2: Farey-Folgen             *
 *  main.cpp                                *
 * ******************************************/
#include <iostream>
#include <string>
#include "rational.h"

using namespace std;

int main()
{
    // Set up test values
    Rational f;

    f.Farey(1);

    f.Farey(2);

    f.Farey(3);

    f.Farey(4);
    
    f.Farey(5);

    f.Farey(6);

    f.Farey(7);

    return 0;
}